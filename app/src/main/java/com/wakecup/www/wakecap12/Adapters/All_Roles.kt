package com.wakecup.www.wakecap12.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.wakecup.www.wakecap12.R
import java.util.ArrayList

class All_Roles(private val name: ArrayList<String>?, private val pic: ArrayList<Int>?, private val context: Context) : RecyclerView.Adapter<All_Roles.SelectedPlayerViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SelectedPlayerViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(R.layout.selected_role, viewGroup, false)
        return SelectedPlayerViewHolder(view)
    }

    override fun onBindViewHolder(selectedPlayerViewHolder: SelectedPlayerViewHolder, i: Int) {

        val pi = pic!![i]
        selectedPlayerViewHolder.s_player_name.text = name!![i] + " " + pi

    }

    override fun getItemCount(): Int {

        return name!!.size

    }

    inner class SelectedPlayerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var s_player_pic: ImageView? = null
        internal var s_player_name: TextView

        init {
            s_player_name = itemView.findViewById(R.id.role_name)
        }
    }


}
