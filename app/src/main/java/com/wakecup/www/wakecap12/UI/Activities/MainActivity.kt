package com.wakecup.www.wakecap12.UI.Activities

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.SyncStateContract
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.airbnb.lottie.LottieAnimationView
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.wakecup.www.wakecap12.Adapters.Worker_Adapter
import com.wakecup.www.wakecap12.DataModel.Workers
import com.wakecup.www.wakecap12.R
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class MainActivity : AppCompatActivity() {
    internal lateinit var requestQueue: RequestQueue
    internal var token = "Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiUlNBLU9BRVAiLCJraWQiOiIzaGltWWEyYmJHS18zSVdZMUxUUHdyTUl2ZkF3ZVhZQmRNLTU5XzN6c0MwIn0.DIwk0Q215_M7rt3iGfyRVrImhKCXvEB5NBoLQj6iqBnN9yCWWBhGW9fmX6beOajtWgC8gpupe1hJ-iurFRbo2Eg52Nh9ljkkO5Mz8ulSpH7YlfP8Yi-TBrNoPIf_IvHrr1Rt8bG0w2Ie1Jo3BBOguMQ6Q2WI4fQDY-oFIluxOQ5t9E0_XvDhna7maiQnWevLbu80Mj7g643v5nvNWOdpSAXW91rZXIXdal_A_ffY-IUyvYhEJuOkyHWwNonuGjiGHWKPTjTR16w3seoSs4srkacOIN1cmMdXKlkvlWSYCv5Nd8xJ_rTqA4GIyKLRSALYcuiTFMOIxeMIpPB5DjpMTOmOD72QnD8MHMx-SNHk2Yva5iPs__h_WXIKzjME6wRQxTBayfWcXuA6loTdSXnV5Tfl63y62nYZUesh2ElcF5ZK_if3R70jTh-_dxAc8-EcxagAgeCAYNGx-i0fokA4Crfz0A9MKJv5XKn8hY-_4_AT-tsIwBQ6YWYISPBGqyUFoBX0LGDEojIywQBXcLSqHRoah45IDW87TTjVTtPjKsxrIr9rl661dYKRE4ViTLGON0hRBmmNAOokcTNv4tEPBWDS5OPIVtnqkCruAV1YXGKMWpbGOlH6pi2sLY5FtNP9h7nNKnK59gX_FMwXyLjzdPU7HkEEv2svTHN_vBFDUeE.UmwA_rn0FGSRXl4kVPrF3Q.lpZ6JQSke2wLCi3X18mNMAYs5x2nCk7LGrs65nOo-cv8FQ1zfYz9qj7TdH26BxmIrtJiBlLkLhJJnWmSjceNsQnCaOFt36cStJKTHbRkydezuBJyIgvlQH-8Y3nTXLpIbo3JQjxTJ-lUwmY8bLnyxS91DaKEjmip3DoXJLSa4uOivuC68npCCtPVFeHbdHrzS_wQTRsUEh1o0XtWI1TtW868mV3njXwtjHdKxp7Uk8wfY5i5R4qJHxXWgf8PuXJA99Hh1Jwzee_tuA67wnPxvKf95mFkZVYZdDqNKrFkDE9B7-r5FjLi3pQZdTMXjFBGAMv20jrM5BuvCZLP9p6ugbG5pOBRUjBT5RtQpw48_th6pTa4W36MkBkh5TbXS27yuOXvi-b3blcSCEiizEDcJvqm4DBwqCNlSR8iG_QgktFDzkG99--LHV4R956K6cAozVAeyv0Jo9mKfaTsRLlMNhfTFpPrZmR5hVb11CEmmLi9C84aK1eivlYOZenzdZKpYE2mBKjZXOl96PJ3jb-OwEO2MDw9fyGzez8NaVhWAGGw9rm1iXnQyGljOw_ufOr5GLPHXXMhjSGVpXbN65sCTv8p6XGZnJiW7otVUzzmxG_kYQ3tkFLTyG9p6SG3GGBDVYG_pkDpPWavWlQdplIDTsOrjoXu-8sAYTx6GO_rGL13haW7KY9pRi9v3324zkNYOYL8CWWZzjcm26607lTEhpVWvkP4QwisGjvrAMoyj-GnlKvyK1u-1sPcuRMUg-8AKRYzBdtlChoUOtdVwYsOHxiLAkRMBH4PRIqWyUNILmlugXGNA7umZxj8uWy2mKjpZ7M8D_YlR66tj4duARp0lJYfeuvDqbXABVuSoLs8qaEFuYXLfiTNH4zItNeMQjfzT_eh9S_ligNALdOb82FgZzYv5ZKbFMo2s9kwzAb4PdwyaZjIP4_UJiJBqOazslWJYpdMLY2ZHGwfg0ZFP3adV06hbR1i9bJ7lyew2Dalqh9Sq-cFPg32-6rbZXYJnFFzfMks4FOJCgPOhCz2QODdbZU8_nQp_EQ4mIXnD2BR6BcCa3odD8rliM3IYOWnq3AhCHen55FwygCw6-u61Q03m4httHFozE-x4YiQivTQ70YUmdYNMyU_chQ3WfiJzaOgEwjq8vIP0hasxiaE1fBG7PncjGCUNB9sFiyVYjmSbP68iIQFqQ10oo9hnMYVxs_nSdem23fDNOutoikzjSHPo4_qxg-hNV0GDIrzBPLGsGw_W9agnUQOpwtpYycZ9v472FWhI5z0c8-8U7D9cV9s0ELyO9U7_vHhSoZJCq1edIFVVKTHsVfR2-vOlCHuqlbH4TzqGtIzf-5nUuY7HQ4iIxdjOrfU2GtMaU_tB8v3ZsC2Nr7IMYeyeBhItOXccNnqHUhMyZ-LdHkoafendmWTnnMOlkK2l91Jil4tPrJyVn1YFlsbzobQOurrsz2MAW3Ew1ibTCsRvlKWRs8dOSAxoUBcQL-r9g5_BcF0wsV7xzWvIHFm7fOuqLGsjsrCMAuXKImA2tFYECpr6vqCq8ORdqapUOlMb0_K4Iae-AhEkbMVERgLS3O05tRhQATMl2jnqA71DImzDoQMC_c45ELXToqX7x0oiINsOLQKBFQjiQ4-G9C1DJlZ67ZVtSN2PyKVNqMNegEtSz1uieOmkoXa1Vq62CrDp7KORToTHMZEV9uWe7KAOzqYm7jwhEvFs2jgvSga9Au2YgzqxbBjpC8RDSQqhUZTQeg2gecFLRpIObQ._YXzEw0gT4HbLqcFfdWoFQ"
    internal var url = "https://pilot.wakecap.com/api/sites/10010001/workers/"
    private var adapter: Worker_Adapter? = null
    private val recyclerView: RecyclerView? = null
    internal lateinit var jsonArray: JSONArray
    internal lateinit var JsonArray_2: JSONObject
    internal lateinit var worker_list: MutableList<Workers>
    internal lateinit var loading: TextView
    internal var toolbar: Toolbar? = null
    private var progressBar: ProgressBar? = null
    internal lateinit var Worker_recycler: RecyclerView
    internal lateinit var animationView: LottieAnimationView
    private var array_role_name: ArrayList<String>? = null
    private var array_all_workers_names:ArrayList<String>? = null
    private var array_role_id: ArrayList<Int>? = null
    private var array_all_role_id: ArrayList<Int>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        animationView = findViewById(R.id.loading_lottie)
        loading = findViewById(R.id.loading)
        progressBar = findViewById(R.id.progressBar_cyclic)
        Worker_recycler = findViewById(R.id.recycler_view_notice_list)
        Worker_recycler.layoutManager = LinearLayoutManager(this)
        worker_list = ArrayList()

        array_role_name = ArrayList()
        array_all_workers_names = ArrayList()
        array_role_id = ArrayList()
        array_all_role_id = ArrayList()

        animationView.setColorFilter(Color.RED)
        requestQueue = Volley.newRequestQueue(this)
        get_data()
    }

    fun show_progress() {

        //		progressBar.setVisibility(View.VISIBLE);
        loading.visibility = View.VISIBLE
        //		getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        //				WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    fun hide_progress() {
        //		progressBar.setVisibility(View.GONE);
        loading.visibility = View.GONE
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        animationView.visibility = View.GONE
    }

    fun get_data() {

        show_progress()

        val postRequest = object : JsonObjectRequest(Request.Method.GET, url, null,
                Response.Listener { response ->
                    Log.d("Response", "" + response.toString())
                    try {
                        val jsonObj = JSONObject(response.toString())
                        val contact = jsonObj.getJSONObject("data")
                        val userJson = contact.getJSONArray("items")

                        userJson.getString(0)
                        Log.d("New Res", "" + userJson.toString())
                        //contact.getString("full_name");

                        Log.d("Names", "" + userJson.toString() + " " + userJson.getString(6))
                        val obj = JSONObject(contact.toString())
                        jsonArray = obj.getJSONArray("items")
                        var count = 0
                        for (i in 0 until userJson.length()) {
                            val jo = jsonArray.getJSONObject(count)

                            JsonArray_2 = jo.getJSONObject("attributes")

                            //								   Toast.makeText(MainActivity.this, ""+Obj_Role, Toast.LENGTH_SHORT).show();
                            val name = JsonArray_2.getString("full_name")
                            val role = JsonArray_2.getString("role")
                            val id = JsonArray_2.getInt("id")
                            //                                   String role_ok=Obj_Role.getString("");
                            val Obj_Role = JsonArray_2.getJSONObject("Role")
                            val js = Obj_Role.getJSONObject("attributes")
                            js.getString("name")
                            //Toast.makeText(MainActivity.this, "Role Name "+js.getString("name"), Toast.LENGTH_SHORT).show();
                            Obj_Role.getString("type")
                            val worker = Workers(
                                    JsonArray_2.getString("full_name"),
                                    JsonArray_2.getInt("id"),
                                    JsonArray_2.getString("role")
                            )

                            array_all_role_id!!.add(js.getInt("id"))
                            array_all_workers_names!!.add(JsonArray_2.getString("full_name"))
                            worker_list.add(worker)
                            if (!array_role_id!!.contains(js.getInt("id"))) {
                                array_role_name!!.add(js.getString("name"))
                                array_role_id!!.add(js.getInt("id"))
                            }
                            // Toast.makeText(MainActivity.this, "Name "+ name+" Role "+role+" ID "+id, Toast.LENGTH_SHORT).show();
                            count = count + 1

                        }

                        Toast.makeText(this@MainActivity, "Role  " + array_role_name.toString() + " IDD " + array_role_id.toString(), Toast.LENGTH_SHORT).show()

                        //							   Log.d("Attribute Array ",""+JsonArray_2.toString());

                        ///// Add adapter

                        adapter = Worker_Adapter(worker_list, this@MainActivity)
                        Worker_recycler.adapter = adapter




                        hide_progress()
                    } catch (e: JSONException) {
                        try {
                            e.printStackTrace()
                            hide_progress()
                        } catch (t: Exception) {
                            hide_progress()
                        }

                    }
                },
                Response.ErrorListener { error ->
                    hide_progress()
                    Toast.makeText(this@MainActivity, "" + error.toString(), Toast.LENGTH_SHORT).show()
                    Log.d("Err", "" + error.toString())
                }) {
            /**
             * Passing some request headers*
             */
            override fun getHeaders(): MutableMap<String, String>? {
                val headers =  HashMap<String,String>()
                headers.put("Content-Type", "application/json")
                headers.put("Authorization", token)
                headers.put("X-Requested-With", "XMLHttpRequest")

                return headers
            }
        }

        val socketTimeout = 30000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        postRequest.retryPolicy = policy

        requestQueue.add(postRequest)


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home_menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.reload) {
            val myIntent = Intent(this@MainActivity, Workers_Grouping::class.java)
            myIntent.putExtra("array_role_names", array_role_name)
            myIntent.putExtra("array_role_ids", array_role_id)
            myIntent.putExtra("array_all_role_id", array_all_role_id)
            myIntent.putExtra("array_all_workers_names", array_all_workers_names)

            this@MainActivity.startActivity(myIntent)

        }

        return false
    }


}
