package com.wakecup.www.wakecap12.UI.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.widget.Toast
import com.chootdev.recycleclick.RecycleClick
import com.wakecup.www.wakecap12.Adapters.All_Roles
import com.wakecup.www.wakecap12.Adapters.Worker_Adapter
import com.wakecup.www.wakecap12.DataModel.Workers
import com.wakecup.www.wakecap12.R
import java.util.ArrayList

class Workers_Grouping : AppCompatActivity() {
    private var array_role_name: ArrayList<String>? = null
    private var array_all_workers_names:ArrayList<String>? = null
    private var array_role_ids: ArrayList<Int>? = null
    private var array_all_role_id:ArrayList<Int>? = null
    internal lateinit var selectedrecycler: RecyclerView
    internal lateinit var recycler_view_worker_role_wise:RecyclerView
    private var adapter: Worker_Adapter? = null
    internal lateinit var worker_list: MutableList<Workers>
    internal lateinit var res: TextView
    internal var dataList: List<Workers>? = null
    internal var num = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_workers__grouping)

        array_role_ids = ArrayList()
        array_all_role_id = ArrayList()
        array_role_name = ArrayList()
        array_all_workers_names = ArrayList()
        worker_list = ArrayList()

        //dataList= new List<Workers>();
        selectedrecycler = findViewById(R.id.selected_player_recycler)
        res = findViewById(R.id.result)
        recycler_view_worker_role_wise = findViewById(R.id.recycler_view_worker_role_wise)

        val extras = intent.extras
        if (extras != null) {

            array_role_name = extras.getStringArrayList("array_role_names")
            array_role_ids = extras.getIntegerArrayList("array_role_ids")
            array_all_role_id = extras.getIntegerArrayList("array_all_role_id")
            array_all_workers_names = extras.getStringArrayList("array_all_workers_names")
            Toast.makeText(this, "T $array_role_name User $array_role_ids", Toast.LENGTH_SHORT).show()
        }



        selectedrecycler.layoutManager = LinearLayoutManager(this@Workers_Grouping, LinearLayoutManager.HORIZONTAL, false)
        selectedrecycler.adapter = All_Roles(array_role_name, array_role_ids, this@Workers_Grouping)

        RecycleClick.addTo(selectedrecycler).setOnItemClickListener { recyclerView, position, v ->
            num = 0
            res.text = ""
            worker_list.clear()
            array_role_ids!!.get(position)
            Toast.makeText(this@Workers_Grouping, "Name " + array_role_name!!.get(position) + " ID " + array_role_ids!!.get(position), Toast.LENGTH_SHORT).show()
            //                dataList.get(position);

            val role_id = array_role_ids!!.get(position)

            for (i in array_all_role_id!!.indices) {
                if (role_id == array_all_role_id!!.get(i)) {
                    num = num + 1
                    res.append(num.toString() + ". " + array_all_workers_names!!.get(i) + " \n ")
                    val worker = Workers(
                            array_all_workers_names!!.get(i),
                            0,
                            array_role_name!!.get(position)
                    )
                    worker_list.add(worker)
                }


            }

            res.text = "Total Employees " + num + "\n Role Name " + array_role_name!!.get(position)

            /// Set Adapter
            recycler_view_worker_role_wise.layoutManager = LinearLayoutManager(this@Workers_Grouping, LinearLayoutManager.VERTICAL, false)
            adapter = Worker_Adapter(worker_list, this@Workers_Grouping)
            recycler_view_worker_role_wise.adapter = adapter

            /// End Adapter
        }


    }



}
