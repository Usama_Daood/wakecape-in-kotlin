package com.wakecup.www.wakecap12.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wakecup.www.wakecap12.R
import com.wakecup.www.wakecap12.DataModel.Workers

class Worker_Adapter
//    public Worker_Adapter(ArrayList<Workers> dataList) {
//        this.dataList = dataList;
//    }

(private val dataList: List<Workers>, internal var context: Context) : RecyclerView.Adapter<Worker_Adapter.NoticeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoticeViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.worker_row, parent, false)
        return NoticeViewHolder(view)
    }

    override fun onBindViewHolder(holder: NoticeViewHolder, position: Int) {
        holder.txtNoticeTitle.setText(""+dataList[position])
        holder.txtNoticeBrief.setText(""+dataList[position])
        //        holder.txtNoticeFilePath.setText(dataList.get(position).getFileSource());
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class NoticeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var txtNoticeTitle: TextView
        var txtNoticeBrief: TextView
        var txtNoticeFilePath: TextView

        init {
            txtNoticeTitle = itemView.findViewById(R.id.txt_notice_title)
            txtNoticeBrief = itemView.findViewById(R.id.txt_notice_brief)
            txtNoticeFilePath = itemView.findViewById(R.id.txt_notice_file_path)
        }
    }


}
