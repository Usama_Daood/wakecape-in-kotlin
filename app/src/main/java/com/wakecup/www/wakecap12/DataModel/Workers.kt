package com.wakecup.www.wakecap12.DataModel

class Workers(Worker_Name: String, Worker_id: Int, worker_role: String) {

    var worker_Name: String
        internal set
    var worker_id: Int = 0
        internal set
    var worker_role: String
        internal set

    init {
        this.worker_Name = Worker_Name
        this.worker_id = Worker_id
        this.worker_role = worker_role
    }


}
